import React, { Fragment, useState } from 'react';
import { useHistory } from "react-router-dom";
const Login = () =>{
    const [userName,setUserName] = useState('')
    const [pwd,setPwd] = useState('')
    const [error,setError] = useState(false)
    const history = useHistory();
    const login = () => {
        //In original scenario an endpoint should be called to validate user
        if(userName === "test" && pwd === "test"){
            sessionStorage.setItem('login',true)
            history.push("/home");
        }else{
            setError(true)
            setTimeout(()=>{
                setError(false)
            },5000)
        }
    }
    const onChangeField = e => {
        switch (e.target.name) {
            case 'username':
                setUserName(e.target.value)
            break;
            case 'password':
                setPwd(e.target.value)
            break;
            default:
                break;
        }
    }
    return(
        <Fragment>
            <span>Username : </span>
            <input onChange={onChangeField} name="username" type="text" />
            <span>Password : </span>
            <input onChange={onChangeField} name="password" type="text" />
            <button onClick={login}>Login</button>
            {error && <span style={{
                display:'block',
                color:'red',
                fontSize:22
            }}>Invalid Credentials</span>}
        </Fragment>
    )
}
export default Login