import React from 'react';
import { useHistory } from 'react-router-dom'
const Dashboard = () =>{
    const history = useHistory()
    const logout = e =>{
        e.preventDefault()
        sessionStorage.removeItem('login')
        history.push('/login')
    }
    return(
        <div>
            <span>You are loggin in</span>
            <button onClick={logout}>Logout</button>
        </div>
    )
}
export default Dashboard