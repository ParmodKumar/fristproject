import React from 'react';
import { Route,Redirect } from 'react-router-dom'
import {isLoggedIn} from '../util'
const PublicRoute = ({component :Component,isRestricted,...rest}) =>{
    return(
        <Route {...rest} render={props=>(
            (isLoggedIn() && isRestricted) ?
                <Redirect to="/home" /> :
                <Component {...props} />
        )} />
    )
}
export default PublicRoute