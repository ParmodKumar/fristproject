import React from 'react';
import '../Singin/Singin.css'
import logosing from '../imges/logosing.jpg';
import Button from '@material-ui/core/Button'

const Singin = () => {
 


    return(
        <React.Fragment>
            <section style={{backgroundImage: "url('" + logosing + "')"}} className="sing-img">
                <div className="login-form">
                    <h3 className="text-center">Welcome Singup</h3>
                        <div className="group-from">
                            <form>
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Email Address"/>
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="Password"  />
                                <div className="btnlogin">
                                    <Button variant="contained" color="secondary" className="longbtn">
                                    Singup
                                    </Button>
                                </div>
                            </form>
                        </div>
                </div>
            </section>
        </React.Fragment>

    )
}

export default Singin;