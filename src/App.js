import React from 'react';
import Login from './components/Login'
import Dashboard from './components/Dashboard'
import About from './components/About'
import { BrowserRouter,Switch } from 'react-router-dom'
import PublicRoute from './components/PublicRoute'
import PrivateRoute from './components/PrivateRoute'
const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <PrivateRoute path="/home" exact component={Dashboard} />
        <PublicRoute isRestricted={true} path="/login" exact component={Login} />
        <PublicRoute isRestricted={true} path="/" exact component={Login} />
        <PublicRoute isRestricted={false} path="/about" component={About} />
      </Switch>
    </BrowserRouter>
  );
}
export default App;